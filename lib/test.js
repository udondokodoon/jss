(function() {
  var BASE_URL, By, driver, webdriver;

  webdriver = require('selenium-webdriver');

  By = webdriver.By;

  BASE_URL = 'http://www.google.com/';

  driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

  driver.get(BASE_URL);

  driver.wait(function() {
    return driver.getTitle().then(function(title) {
      return title === 'webdriver - Google Search';
    }, 2000);
  });

  driver.quit();

}).call(this);
