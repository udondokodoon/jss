(function() {
  var JSPP, Transform,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  require('source-map-support').install();

  Transform = require('stream').Transform;

  JSPP = (function(_super) {
    __extends(JSPP, _super);

    function JSPP(options) {
      JSPP.__super__.constructor.call(this, options);
      this.data = '';
      ({
        _transform: function(chunk, output, cb) {
          chunk = chunk.toString();
          if (chunk) {
            this.data += chunk;
          }
          return cb(null);
        },
        _flush: function(output, cb) {
          var error;
          if (this.data) {
            try {
              output(this._pp(this.data));
              return cb(null);
            } catch (_error) {
              error = _error;
              return cb(error);
            }
          }
        },
        _pp: function(jsonString) {
          return JSON.stringify(JSON.parse(jsonString), null, 2) + '\n';
        }
      });
    }

    return JSPP;

  })(Transform);

  module.exports.JSPP = JSPP;

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpzcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBLGVBQUE7SUFBQTttU0FBQTs7QUFBQSxFQUFBLE9BQUEsQ0FBUSxvQkFBUixDQUE2QixDQUFDLE9BQTlCLENBQUEsQ0FBQSxDQUFBOztBQUFBLEVBQ0EsU0FBQSxHQUFZLE9BQUEsQ0FBUSxRQUFSLENBQWlCLENBQUMsU0FEOUIsQ0FBQTs7QUFBQSxFQUtNO0FBQ0wsMkJBQUEsQ0FBQTs7QUFBYSxJQUFBLGNBQUMsT0FBRCxHQUFBO0FBQ1osTUFBQSxzQ0FBTSxPQUFOLENBQUEsQ0FBQTtBQUFBLE1BQ0EsSUFBQyxDQUFBLElBQUQsR0FBUSxFQURSLENBQUE7QUFBQSxNQUdBLENBQUE7QUFBQSxRQUFBLFVBQUEsRUFBWSxTQUFDLEtBQUQsRUFBUSxNQUFSLEVBQWdCLEVBQWhCLEdBQUE7QUFDVixVQUFBLEtBQUEsR0FBUSxLQUFLLENBQUMsUUFBTixDQUFBLENBQVIsQ0FBQTtBQUNBLFVBQUEsSUFBRyxLQUFIO0FBQ0UsWUFBQSxJQUFDLENBQUEsSUFBRCxJQUFTLEtBQVQsQ0FERjtXQURBO2lCQUlBLEVBQUEsQ0FBRyxJQUFILEVBTFU7UUFBQSxDQUFaO0FBQUEsUUFPQSxNQUFBLEVBQVEsU0FBQyxNQUFELEVBQVMsRUFBVCxHQUFBO0FBQ04sY0FBQSxLQUFBO0FBQUEsVUFBQSxJQUFHLElBQUMsQ0FBQSxJQUFKO0FBQ0U7QUFDRSxjQUFBLE1BQUEsQ0FBTyxJQUFJLENBQUMsR0FBTCxDQUFTLElBQUMsQ0FBQSxJQUFWLENBQVAsQ0FBQSxDQUFBO3FCQUNBLEVBQUEsQ0FBRyxJQUFILEVBRkY7YUFBQSxjQUFBO0FBSUUsY0FESSxjQUNKLENBQUE7cUJBQUEsRUFBQSxDQUFHLEtBQUgsRUFKRjthQURGO1dBRE07UUFBQSxDQVBSO0FBQUEsUUFlQSxHQUFBLEVBQUssU0FBQyxVQUFELEdBQUE7QUFDSCxpQkFBTyxJQUFJLENBQUMsU0FBTCxDQUFlLElBQUksQ0FBQyxLQUFMLENBQVcsVUFBWCxDQUFmLEVBQXVDLElBQXZDLEVBQTZDLENBQTdDLENBQUEsR0FBa0QsSUFBekQsQ0FERztRQUFBLENBZkw7T0FBQSxDQUhBLENBRFk7SUFBQSxDQUFiOztnQkFBQTs7S0FEa0IsVUFMbkIsQ0FBQTs7QUFBQSxFQTZCQSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQWYsR0FBc0IsSUE3QnRCLENBQUE7QUFBQSIsImZpbGUiOiJqc3MuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlKCdzb3VyY2UtbWFwLXN1cHBvcnQnKS5pbnN0YWxsKClcblRyYW5zZm9ybSA9IHJlcXVpcmUoJ3N0cmVhbScpLlRyYW5zZm9ybVxuXG5cblxuY2xhc3MgSlNQUCBleHRlbmRzIFRyYW5zZm9ybVxuXHRjb25zdHJ1Y3RvcjogKG9wdGlvbnMpLT5cblx0XHRzdXBlciBvcHRpb25zXG5cdFx0QGRhdGEgPSAnJ1xuXG4gIF90cmFuc2Zvcm06IChjaHVuaywgb3V0cHV0LCBjYiktPlxuICAgIGNodW5rID0gY2h1bmsudG9TdHJpbmcoKVxuICAgIGlmIGNodW5rXG4gICAgICBAZGF0YSArPSBjaHVua1xuXG4gICAgY2IobnVsbCk7XG5cbiAgX2ZsdXNoOiAob3V0cHV0LCBjYiktPlxuICAgIGlmIEBkYXRhXG4gICAgICB0cnlcbiAgICAgICAgb3V0cHV0IHRoaXMuX3BwIEBkYXRhXG4gICAgICAgIGNiKG51bGwpXG4gICAgICBjYXRjaCBlcnJvclxuICAgICAgICBjYihlcnJvcilcblxuICBfcHA6IChqc29uU3RyaW5nKS0+XG4gICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KEpTT04ucGFyc2UoanNvblN0cmluZyksIG51bGwsIDIpICsgJ1xcbic7XG5cblxubW9kdWxlLmV4cG9ydHMuSlNQUCA9IEpTUFAiXX0=