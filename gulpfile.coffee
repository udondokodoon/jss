gulp = require 'gulp'
coffee = require 'gulp-coffee'
gutil = require 'gulp-util'
sourcemaps = require 'gulp-sourcemaps'


gulp.task 'watch', ->
	gulp.watch './src/**/*.coffee'
		.on 'change', (changed)->
			gulp.src changed.path
				.pipe sourcemaps.init()
				.pipe coffee()
				.pipe sourcemaps.write()
				.pipe gulp.dest './lib'

gulp.task 'default', ['watch']